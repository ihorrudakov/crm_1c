package utils;

public class Crm1cDataExportException extends RuntimeException {

    public Crm1cDataExportException() {
        super();
    }

    public Crm1cDataExportException(String text) {
        super(text);
    }
}
