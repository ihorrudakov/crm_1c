import org.testng.annotations.Test;
import utils.Crm1cDataExportException;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class SmokeTest extends MainTest {

    @Test(alwaysRun = true)
    public void check1cExportDataStatus4() throws SQLException {
        Statement st = conn.createStatement();
        ResultSet checking = st.executeQuery("SELECT count(*) FROM crm_stage.1c_data_export where status = 4 and directoryName != 16;");
        checking.next();
        if (checking.getInt(1) > 0) {
            ResultSet rs = st.executeQuery("SELECT id, entityType, entityId, documentName, directoryName, status FROM crm_stage.1c_data_export where status = 4 and directoryName != 16;");
            String temp = "";
            int i = 0;
            temp += "\nЗаписи из таблицы 1c_data_export, неудовлетворяющие условиям (статус 4):";
            temp += String.format("%n%-10s%-27s%-15s%-35s%-15s%-10s%n", "ID", "EntityType", "EntityID","DocumentName", "DirectoryId", "Status");
            temp += String.format("------------------------------------------------------------------------------------------------------------\n");
            while (rs.next()) {
                temp += String.format("%-10s%-27s%-15s%-35s%-15s%-10s%n", rs.getInt(1), rs.getString(2), rs.getString(3),
                        rs.getString(4), rs.getString(5), rs.getString(6));
                i++;
            }
            temp += "\nКоличество записей: " + i;
            throw new Crm1cDataExportException(temp);
        }
    }

    @Test(alwaysRun = true)
    public void check1cExportDataStatus01() throws SQLException {
        Statement st = conn.createStatement();
        ResultSet checking = st.executeQuery("SELECT * FROM crm_stage.1c_data_export where (status = 0 and directoryName != 16) or (status = 1 and directoryName != 16);");
        try {
            checking.next();
            if (checking.getInt(1) > 0) {
                ResultSet rs = st.executeQuery("SELECT id, entityType, entityId, documentName, directoryName, status, createdAt FROM crm_stage.1c_data_export where (status = 0 and directoryName != 16) or (status = 1 and directoryName != 16);");
                String temp = "";
                int i = 0;
                temp += "\nЗаписи из таблицы 1c_data_export со статусами 0 и 1:";
                temp += String.format("%n%-10s%-27s%-15s%-35s%-15s%-9s%-20s%n", "ID", "EntityType", "EntityID","DocumentName", "DirectoryId", "Status", "CreatedAt");
                temp += String.format("------------------------------------------------------------------------------------------------------------------------------------\n");
                while (rs.next()) {
                    temp += String.format("%-10s%-27s%-15s%-35s%-15s%-9s%-20s%n", rs.getInt(1), rs.getString(2), rs.getString(3),
                            rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7));
                    i++;
                }
                temp += "\nКоличество записей: " + i;
                throw new Crm1cDataExportException(temp);
            }
        } catch (SQLException e) {
            System.out.println("Все хорошо!");
        }

    }
}
