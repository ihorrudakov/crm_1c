import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;

import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public abstract class MainTest {

    String dbHost = "62.109.21.167";
    int dbPort = 3306;
    String dbuserName = "crm_readonly";
    String dbpassword = "R2zITNGrszwdI3Qu";

    //String dbuserName = "igor_readonly";
    //String dbpassword = "CL1ESSZMzH2EKoP9";

    String url = "jdbc:mysql://188.120.233.103:3306/crm_stage?verifyServerCertificate=false&useSSL=true&requireSSL=true";
    String driverName = "com.mysql.jdbc.Driver";
    Connection conn = null;

    public void createDBconnection() {
        try {
            //mysql database connectivity
            Class.forName(driverName).newInstance();
            conn = DriverManager.getConnection(url, dbuserName, dbpassword);

            System.out.println("Database connection established");
            System.out.println("DONE");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void closeDBconnection() throws SQLException {
        if (conn != null && !conn.isClosed()){
            System.out.println("Closing Database Connection");
            conn.close();
        }
    }

    @BeforeTest
    public void before() throws UnsupportedEncodingException {
        createDBconnection();
    }

    @AfterTest
    public void after() throws SQLException {
        closeDBconnection();
    }
}
